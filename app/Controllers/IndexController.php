<?php
namespace App\Controllers;

if (!session_id()) @session_start();

use OLX\Controller\Action;
use OLX\DI\Container;

class IndexController extends Action
{
    ################################################################
    # Método responsável por exibir a view e tratar os dados do POST
    ################################################################
    public function index()
    {
    	$modelClient = Container::getModel("Client");
    	$flashMessage = new \Plasticbrain\FlashMessages\FlashMessages();
    	$this->views->paises = [
			1 => 'Portugal',
			2 => 'Brasil',
			3 => 'Canadá',
			4 => 'Espanha'
		];

		// verifico se veio o token
        if (isset($_POST['csrf_token']) && $_POST['csrf_token'] === $_SESSION['csrf_token']) {
			$error = [];
    		// inicio algumas validações
    		// o correto é estar no Model, mas para não deixar a estrutura mais complexa vou manter aqui
    		if(!$this->validaCampo($_POST['email']) || !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))
    			array_push($error, 'Email inválido.');
    		if(!$this->validaCampo($_POST['cemail']) || !filter_var($_POST['cemail'], FILTER_VALIDATE_EMAIL) || $_POST['cemail'] !== $_POST['email'])
    			array_push($error, 'Confirmação do Email inválida.');

    		if(!$this->validaCampo($_POST['password']) || strlen($_POST['password']) < 6)
    			array_push($error, 'O Password deve conter ao menos 6 caracteres.');
    		if(!$this->validaCampo($_POST['cpassword']) || strlen($_POST['cpassword']) < 6 || $_POST['cpassword'] !== $_POST['password'])
    			array_push($error, 'Confirmação de Password inválida.');

    		if(!$this->validaCampo($_POST['nome'], 4) || strlen($_POST['nome']) < 4)
    			array_push($error, 'O Nome deve conter ao menos 4 caracteres.');

    		if(!$this->validaCampo($_POST['apelido']) || strlen($_POST['apelido']) < 2)
    			array_push($error, 'O Apelido deve conter ao menos 2 caracteres.');

    		if(!$this->validaCampo($_POST['nif']) && strlen($_POST['nif']) !== 9)
    			array_push($error, 'O NIF deve possuir 9 números.');

    		// se falhou nas validacoes basicas, já exibo as mensagens de erro
    		if(!empty($error))
    		{
    			$flashMessage->error(implode('<br />', $error));
    			$this->views->messages = $flashMessage->display('error');
    			$this->render("index");
    			return;
    		}

    		// sanitizo alguns dados para evitar injections
    		$sanitize = [
    			'email' => FILTER_SANITIZE_EMAIL,
    			'password' => FILTER_SANITIZE_STRING,
    			'nome' => FILTER_SANITIZE_STRING,
    			'apelido' => FILTER_SANITIZE_STRING,
    			'rua' => FILTER_SANITIZE_STRING,
    			'codigo_postal' => FILTER_SANITIZE_NUMBER_INT,
    			'localidade' => FILTER_SANITIZE_STRING,
    			'id_pais' => FILTER_SANITIZE_NUMBER_INT,
    			'nif' => FILTER_SANITIZE_STRING,
    			'telefone' => FILTER_SANITIZE_STRING,
    		];
    		foreach($sanitize as $k => $v)
    			$_POST[$k] = filter_var($_POST[$k], $v);

    		// se chegou ate aqui, os dados estao integros
    		$res = $modelClient->saveClient($_POST);
    		if($res === true)
    		{
    			// redirect com sucesso
    			$flashMessage->success('Cliente cadastrado com sucesso.', '/');
    		}
			
			$flashMessage->error($res);
			$this->views->messages = $flashMessage->display('error');
		}

		// garanto o token no formulario
    	if (! isset($_SESSION['csrf_token'])) {
    		$_SESSION['csrf_token'] = base64_encode(openssl_random_pseudo_bytes(32));
		}

		// carrego a view
        $this->render("index");
    }

    #################################################################
    # Método responsável por validar se um campo está setado/valorado
    #################################################################
    public function validaCampo($valor)
    {
    	return (!isset($valor) || is_null(trim($valor))) ? false : true;
    }

    ############################################################################
    # Método responsável por verificar se um email está sendo utilizado no banco
    ############################################################################
    public function validar()
    {
    	if (isset($_POST['csrf_token']) && $_POST['csrf_token'] === $_SESSION['csrf_token']) {
	    	$email = $_POST['email'];
	    	$modelClient = Container::getModel("Client");
	    	echo json_encode($modelClient->isUnique($email));
		}
    }

}