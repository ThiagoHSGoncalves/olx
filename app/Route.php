<?php

namespace App;

use OLX\Init\Bootstrap;

class Route extends Bootstrap
{

    protected function initRoutes()
    {
        $routes['home'] = array("route"=>'/',"controller"=>"indexController","action"=>"index");
        $routes['validar'] = array("route"=>'/validar',"controller"=>"indexController","action"=>"validar");
        $this->setRoute($routes);
    }

}