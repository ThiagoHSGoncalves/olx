<?php

namespace App\Models;

use OLX\Model\Table;

class Client extends Table
{
    protected $table = "clientes";
}