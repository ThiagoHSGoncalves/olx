CREATE DATABASE `olx` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE olx;

CREATE TABLE `paises` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) NOT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

INSERT INTO `paises` VALUES (1,'Portugal',1),(2,'Brasil',1),(3,'Canadá',1),(4,'Espanha',1);

CREATE TABLE `clientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `apelido` varchar(30) NOT NULL,
  `rua` varchar(100) DEFAULT NULL,
  `codigo_postal` varchar(8) DEFAULT NULL,
  `localidade` varchar(50) DEFAULT NULL,
  `nif` varchar(9) DEFAULT NULL,
  `telefone` varchar(20) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_pais` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  KEY `pais_cliente_idx` (`id_pais`),
  CONSTRAINT `pais_cliente` FOREIGN KEY (`id_pais`) REFERENCES `paises` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

