<?php

namespace OLX\Model;


abstract class Table
{
    protected $db;
    protected $table;

    public function __construct(\PDO $db)
    {
        $this->db = $db;
    }

    public function fetchAll()
    {
        $query = "select * from {$this->table}";
        return $this->db->query($query);
    }

    public function find($id)
    {
        $query = "select * from {$this->table} WHERE id=:id";
        $stmt = $this->db->prepare($query);
        $stmt->bindParam(":id",$id);
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_ASSOC);

    }

    public function saveClient($data)
    {
        if(!$this->isUnique($data['email']))
            return 'Este email já está sendo utilizado, por favor, utilize outro.';

        $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
        $data['id_pais'] = intval($data['id_pais']);
        $fields = [
            'email',
            'password',
            'nome',
            'apelido',
            'rua',
            'codigo_postal',
            'localidade',
            'id_pais',
            'nif',
            'telefone'
        ];

        $values  = [];
        $valStmt = [];
        foreach($fields as $k)
        {
            $values[$k] = "'{$data[$k]}'";
        }

        // isso está porco
        // mas o bindParam estava truncando o id_pais e o prazo para entregar já estourou
        // então vai assim, ao menos funciona
        // depois eu refatoro isso
        $query = "insert into clientes (email, password, nome, apelido, rua, codigo_postal, localidade, id_pais, nif, telefone) 
                    values 
                ({$values['email']}, {$values['password']}, {$values['nome']}, {$values['apelido']}, {$values['rua']}, {$values['codigo_postal']}, {$values['localidade']}, {$values['id_pais']}, {$values['nif']}, {$values['telefone']})";
        
        $this->db->setAttribute( \PDO::ATTR_ERRMODE, \PDO::ERRMODE_WARNING );
        $stmt = $this->db->prepare($query);
        $res = $stmt->execute();
        return ($res) ? $res : $stmt->errorInfo()[2];
    }

    public function isUnique($email)
    {
        $query = $this->db->prepare("SELECT email FROM clientes WHERE email = ?");
        $query->bindValue( 1, $email );
        $query->execute();

        return ($query->rowCount() > 0) ? false : true;
    }
}