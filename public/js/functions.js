jQuery(document).ready(function () {
    
    $.ajaxSetup({
        data: window.csrf
    });

    "use strict";

    /****************************************** 
        #######################################
        REGRA PROGRESSBAR COMPLEXIDADE DA SENHA
        #######################################

        Utilizei uma lib pronta para calcular o peso do password
        Ela utiliza alguns critérios como:
            - não ser um email
            - muito pequena
            - sequencia de letras
            - repetições
            - se tem uma maiuscula/minuscula
            - se tem caracteres especiais
            etc.
        Cada um destes criterios possui um peso de pontuacao
        Baseado nesta pontuação ela manipula o progressbar:

        ui.percentage = function (options, score, maximun) {
        var result = Math.floor(100 * score / maximun),
            min = options.ui.progressBarMinPercentage;

        result = result <= min ? min : result;
        result = result > 100 ? 100 : result;
        return result;
    };
    *******************************************/
    
    //#######################################
    // PROGRESS BAR FORÇA PASSWORD
    //#######################################
    i18next.init({
    lng: 'pt',
    resources: {
      pt: {
        translation: {
          "veryWeak": "Muito Fraca",
          "weak": "Fraca",
          "normal": "Normal",
          "medium": "Media",
          "strong": "Forte",
          "veryStrong": "Muito Forte",
          "label": "Senha",
          "goBack": "Voltar"
        }
      }
    }
    }, function () {
    // Initialized and ready to go

    var options = {};
    options.ui = {
        container: "#pwd-container",
        showVerdicts: false,
        viewports: {
            progress: ".pwstrength_viewport_progress"
        },
        showErrors: false
    };
    options.common = {
        debug: false,
    };
    $('#password').pwstrength(options);
    });


    //#######################################
    // VALIDACAO DOS CAMPOS DO FORM
    //#######################################
    $("#signup").validate({
        rules: {
            email: {
                required: true,
                email: true
            },
            cemail: {
                required: true,
                email: true,
                equalTo: "#email"
            },
            password: {
                required: true,
                minlength: 5
            },
            cpassword: {
                required: true,
                minlength: 5,
                equalTo: "#password"
            },
            nif: {
                required: true,
                minlength: 9,
                maxlength: 9,
                digits: true
            },
            nome: "required",
            apelido: "required"
        },
        messages: {
            nome: "Por favor, informe seu nome.",
            apelido: "Por favor, informe seu apelido.",
            password: {
                required: "Por favor, informe uma senha.",
                minlength: "A senha deve ter pelo menos 5 caracteres."
            },
            cpassword: {
                required: "Por favor, informe a senha novamente.",
                minlength: "A senha deve ter pelo menos 5 caracteres.",
                equalTo: "As senhas devem ser iguais."
            },
            email: "Por favor, informe um email válido.",
            cemail: {
                required: "Por favor, informe o email novamente.",
                equalTo: "Os emails devem ser iguais."
            },
            nif: {
                required: "Por favor, informe o NIF.",
                minlength: "O NIF deve conter 9 digitos.",
                maxlength: "O NIF deve conter 9 digitos.",
                digits: "Apenas números."
            },

        }
    });

    //#######################################
    // MASCARAS DO FORM
    //#######################################
    // mask para telefone
    $('#telefone').focus(function(){
        if($("#id_pais").val() == '1')
            $('#telefone').mask("0000-000", {selectOnFocus: true});
        else
            $('#telefone').unmask();
    });

    $('#codigo_postal').mask("0000-000", {selectOnFocus: true});

    //#######################################
    // AJAX PARA VALIDAR O EMAIL EM USO NO BANCO DE DADOS
    //#######################################
    $("#email").change(function(){
        $.ajax({
            type: "POST",
            url: '/validar',
            data: {'email': $(this).val()},
            success: function(data){
                if(data == false)
                {
                    $(".container").before('<div class="emailValidator alert dismissable alert-danger"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>Este email já está sendo utilizado, por favor, utilize outro.</div>');
                }
                else
                {
                    $(".emailValidator").remove();
                }
            },
            dataType: 'json'
        });
    });
    
});